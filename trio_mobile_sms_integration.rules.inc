<?php

/**
 * Implements hook_rules_action_info().
 */
function trio_mobile_sms_integration_rules_action_info() {
    $actions = array(
      'trio_mobile_sms_integration_action_send_sms' => array(
        'label' => t('Send Sms with PET mails'),
        'group' => t('Previewable email templates'),
        'parameter' => array(
          'pet_name' => array(
            'type' => 'text',
            'label' => t('Template to use for SMS'),
            'options list' => 'commerce_pet_sms_list',
            'description' => t('The template that will be sent for this action. You can see the full list or add a new one from <a href="@url">this page</a>.', array('@url' => url('admin/structure/pets'))),
          ),
          'to_text' => array(
            'type' => 'text',
            'label' => t('Recipient(s) (for sending sms to a fixed set of Phone Numbers'),
            'description' => t('Phone Number of the Recipents'),
            'optional' => TRUE,
            'default value' => NULL,
          ),
          'entity_subs' => array(
            'type' => 'entity',
            'label' => t('Entity for token substitutions (if any)'),
            'description' => t('If your template includes entity tokens, this entity will be used for them.'),
            'optional' => TRUE,
            'default value' => NULL,
          ),
        ),
      ),
    );
    return $actions;
}

/**
 * Callback for  rules action.
 */
function trio_mobile_sms_integration_action_send_sms($pet_name, $to_text, $entity_subs) {
    $pet = pet_load($pet_name);
//call to sms api
    trio_mobile_sms_integration_api($pet, $to_text, $entity_subs);
}

/**
 * Return list of all PETs for rules configuration.
 */
function commerce_pet_sms_list() {
    $list = array();
    foreach (pet_load_multiple(FALSE) as $pet) {
        $list[$pet->name] = $pet->title;
    }
    return $list;
}
