<?php

/**
 * Admin Configuration form SMS API
 */
function trio_mobile_sms_integration_config_settings($form, $form_state) {

    $form['sms_api'] = array(
      '#type' => "textfield",
      '#title' => t("Enter API Key"),
      "#description" => t("Enter API key of SMS Gateway Used"),
      '#default_value' => variable_get('sms_api', array()),
    );
    $form['api_url'] = array(
      '#type' => "textfield",
      '#title' => t("Enter API URL"),
      "#description" => t("Enter API URL of SMS Gateway"),
      '#default_value' => variable_get('api_url', array()),
    );
    $form['sender_id'] = array(
      '#type' => "textfield",
      '#title' => t("Enter Sender ID"),
      "#description" => t("Enter Sender ID(eg. CLOUDSMS)"),
      '#default_value' => variable_get('sender_id', array()),
    );
    $form['sms_mode'] = array(
      '#type' => 'select',
      '#title' => t('Select Mode'),
      '#options' => array(
        'shortcode' => t('shortcode'),
        'longcode' => t('longcode'),
      ),
      '#default_value' => variable_get('sms_mode', array()),
      '#description' => t('Set this to <em>Shortcode</em> if you want to send international SMS.'),
    );

    return system_settings_form($form);
}
